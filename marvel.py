from time import time
from hashlib import md5
from requests import get

PUBKEY = 'd76a3b798c777132079137f35461b20e'
PRIVKEY = 'c1a42df48fc2687176454d8da52fac8e5810473f'


def _hash_(pubkey, privkey):
	ts = int(time())
	return (ts, pubkey, md5('%i%s%s' % (ts, privkey, pubkey)).hexdigest())

class CharactersAPI(object):
	'''API to v1/public/characters features
	'''

	def __init__(self, pubkey, privkey):
		self.params = 'ts=%s&apikey=%s&hash=%s' % _hash_(pubkey, privkey)
		self.url = "http://gateway.marvel.com/v1/public/characters"

	def request(self, *args, **kwargs):
		_args_ = '/'.join([str(i) for i in args])
		_kwargs_ = '&'.join(['%s=%s' % (k, v) for k, v in kwargs.iteritems()])

		if _args_:
			url = '%s/%s?%s' % (self.url, _args_, self.params)

		if _kwargs_:
			url = '%s?%s&%s' % (self.url, self.params, _kwargs_)

		if _args_ and _kwargs_:
			url = '%s/%s?%s&%s' % (self.url, _args_, self.params, _kwargs_)

		if not _args_ and not _kwargs_:
			url = '%s?%s' % (self.url, self.params)

		return get(url)

	def characters(self, **kwargs):
		return self.request(**kwargs)

	def character(self, id, **kwargs):
		return self.request(*[id,])

	def comics(self, id, **kwargs):
		return self.request(*[id, 'comics'], **kwargs)

	def events(self, id, **kwargs):
		return self.request(*[id, 'events'], **kwargs)

	def series(self, id, **kwargs):
		return self.request(*[id, 'series'], **kwargs)

	def stories(self, id, **kwargs):
		return self.request(*[id, 'stories'], **kwargs)


class ComicsAPI(object):
	'''API to v1/public/comics features
	'''

	def __init__(self, pubkey, privkey):
		self.params = 'ts=%s&apikey=%s&hash=%s' % _hash_(pubkey, privkey)
		self.url = "http://gateway.marvel.com/v1/public/comics"

	def request(self, *args, **kwargs):
		_args_ = '/'.join([str(i) for i in args])
		_kwargs_ = '&'.join(['%s=%s' % (k, v) for k, v in kwargs.iteritems()])

		if _args_:
			url = '%s/%s?%s' % (self.url, _args_, self.params)

		if _kwargs_:
			url = '%s?%s&%s' % (self.url, self.params, _kwargs_)

		if _args_ and _kwargs_:
			url = '%s/%s?%s&%s' % (self.url, _args_, self.params, _kwargs_)

		if not _args_ and not _kwargs_:
			url = '%s?%s' % (self.url, self.params)

		return get(url)

	def comics(self, **kwargs):
		return self.request(**kwargs)

	def comic(self, id, **kwargs):
		return self.request(*[id,])

	def characters(self, id, **kwargs):
		return self.request(*[id, 'characters'], **kwargs)

	def creators(self, id, **kwargs):	
		return self.request(*[id, 'creators'], **kwargs)

	def events(self, id, **kwargs):
		return self.request(*[id, 'events'], **kwargs)

	def stories(self, id, **kwargs):
		return self.request(*[id, 'stories'], **kwargs)