from json import loads
from unittest import TestCase, skip
from mock import patch
from ..marvel import ComicsAPI, PUBKEY, PRIVKEY


class TestComics(TestCase):

	def setUp(self):
		self.pubkey = PUBKEY
		self.privkey = PRIVKEY
		self.comics = ComicsAPI(self.pubkey, self.privkey)

	def loadjson(self, jfp):
		with open(jfp, 'rb') as j:
			return loads(j.read())

	@patch('hulk.marvel.get')
	def test_get_cpmic_list_should_succeed(self, mock_requests):
		'''List all comics by the API limit results'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_comics.json')
		self.assertEqual(self.comics.comics(), self.loadjson('tests/resources/comics_comics.json'))


	@patch('hulk.marvel.get')
	def test_get_character_by_id_should_succeed(self, mock_requests):
		'''Get comic info by id'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_id.json')
		self.assertEqual(self.comics.comic(41530), self.loadjson('tests/resources/comics_id.json'))

	@patch('hulk.marvel.get')
	def test_comics_list_from_character_by_id_should_succeed(self, mock_requests):
		'''Get char list by comics id'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_chars.json')
		self.assertEqual(self.comics.characters(41530), self.loadjson('tests/resources/comics_chars.json'))

	@patch('hulk.marvel.get')
	def test_comics_list_by_creators_should_succeed(self, mock_requests):
		'''Get creators list by comic id'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_creators.json')
		self.assertEqual(self.comics.creators(1009351), self.loadjson('tests/resources/comics_creators.json'))

	@patch('hulk.marvel.get')
	def test_list_events_from_comic_by_id_should_succeed(self, mock_requests):
		'''Get events list by comic id'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_events.json')
		self.assertEqual(self.comics.events(1009351), self.loadjson('tests/resources/comics_events.json'))

	@patch('hulk.marvel.get')
	def test_list_stories_from_comic_by_id_should_succeed(self, mock_requests):
		'''Get stories list by comic id'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_stories.json')
		self.assertEqual(self.comics.stories(1009351), self.loadjson('tests/resources/comics_stories.json'))