from json import loads
from unittest import TestCase, skip
from mock import patch
from ..marvel import CharactersAPI, PUBKEY, PRIVKEY


class TestCharacters(TestCase):

	def setUp(self):
		self.pubkey = PUBKEY
		self.privkey = PRIVKEY
		self.chars = CharactersAPI(self.pubkey, self.privkey)

	def loadjson(self, jfp):
		with open(jfp, 'rb') as j:
			return loads(j.read())

	@patch('hulk.marvel.get')
	def test_get_characters_list_should_succeed(self, mock_requests):
		'''List all chars by the API limit results'''
		mock_requests.return_value = self.loadjson('tests/resources/chars.json')
		self.assertEqual(self.chars.characters(), self.loadjson('tests/resources/chars.json'))

	@patch('hulk.marvel.get')
	def test_find_by_name_in_characters_list_should_succeed(self, mock_requests):
		'''Get char info by name'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_name.json')
		self.assertEqual(self.chars.characters(**{'name': 'Hulk'}), self.loadjson('tests/resources/chars_name.json'))

	@patch('hulk.marvel.get')
	def test_get_character_by_id_should_succeed(self, mock_requests):
		'''Get char info by id'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_id.json')
		self.assertEqual(self.chars.character(1009351), self.loadjson('tests/resources/chars_id.json'))

	@patch('hulk.marvel.get')
	def test_comics_list_from_character_by_id_should_succeed(self, mock_requests):
		'''Get comics list by char id'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_comics.json')
		self.assertEqual(self.chars.comics(1009351), self.loadjson('tests/resources/chars_comics.json'))

	@patch('hulk.marvel.get')
	def test_comics_list_from_character_by_startYear_should_succeed(self, mock_requests):
		'''Get comics list by char id and using params'''
		mock_requests.return_value = self.loadjson('tests/resources/comics_by_startyear.json')
		self.assertEqual(self.chars.comics(1009351, **{'startYear': 2014}), self.loadjson('tests/resources/comics_by_startyear.json'))

	@patch('hulk.marvel.get')
	def test_list_events_from_character_by_id_should_succeed(self, mock_requests):
		'''Get events list by char id'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_events.json')
		self.assertEqual(self.chars.events(1009351), self.loadjson('tests/resources/chars_events.json'))

	@patch('hulk.marvel.get')
	def test_list_series_from_character_by_id_should_succeed(self, mock_requests):
		'''Get series list by char id'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_series.json')
		self.assertEqual(self.chars.series(1009351), self.loadjson('tests/resources/chars_series.json'))

	@patch('hulk.marvel.get')
	def test_list_stories_from_character_by_id_should_succeed(self, mock_requests):
		'''Get stories list by char id'''
		mock_requests.return_value = self.loadjson('tests/resources/chars_stories.json')
		self.assertEqual(self.chars.stories(1009351), self.loadjson('tests/resources/chars_stories.json'))
